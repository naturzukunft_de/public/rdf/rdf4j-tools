package de.naturzukunft.rdf4j.ommapper;

import java.math.BigInteger;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;

import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Iri(TestPojo.NAMESPACE)
public class TestPojo extends BaseObject {
	public static final String NAMESPACE = "http://example.com/testPojo/";
	
	@Iri(NAMESPACE + "Iri")
	private IRI iri;

	@Iri(NAMESPACE + "IriSet")
	private Set<IRI> iriSet;

	@Iri(NAMESPACE + "String")
	@de.naturzukunft.rdf4j.ommapper.NonNull
	private String name;
	
	@Iri(NAMESPACE + "Boolean")
	private Boolean myBoolean;

	@Iri(NAMESPACE + "byte")
	private byte myPrimitiveByte;

	@Iri(NAMESPACE + "Byte")
	private Byte myByte;

//	@Iri(NAMESPACE + "char")
//	private char mychar;
	
//	@Iri(NAMESPACE + "Char")
//	private Char myChar;

	@Iri(NAMESPACE + "double")
	private double myPrimitiveDouble;
	
	@Iri(NAMESPACE + "Double")
	private double myDouble;

	@Iri(NAMESPACE + "float")
	private float myPrimitiveFloat;
	
	@Iri(NAMESPACE + "Float")
	private float myFloat;
	
	@Iri(NAMESPACE + "int")
	private int myPrimitiveInt;
	
	@Iri(NAMESPACE + "Int")
	private int myInt;

	@Iri(NAMESPACE + "long")
	private long myPrimitiveLong;
	
	@Iri(NAMESPACE + "Long")
	private long myLong;
	
	@Iri(NAMESPACE + "short")
	private short myPrimitiveShort;
	
	@Iri(NAMESPACE + "Short")
	private Short myShort;
	
	@Iri(NAMESPACE + "BigInteger")
	private BigInteger myBigInt;
	
	@Iri(NAMESPACE + "LocalDateTime")
	private LocalDateTime localDateTime;

	@Iri(NAMESPACE + "Duration")
	private Duration duration;

	@Iri(AS.NAMESPACE + "object")
	private ReferencedPojo referencedPojo;
}
