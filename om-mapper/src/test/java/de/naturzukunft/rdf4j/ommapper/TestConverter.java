package de.naturzukunft.rdf4j.ommapper;

//import static org.eclipse.rdf4j.model.util.Values.iri;
//import static org.hamcrest.CoreMatchers.is;
//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertThrows;
//
//import java.io.StringWriter;
//import java.math.BigInteger;
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//import java.util.Calendar;
//import java.util.Set;
//
//import javax.xml.datatype.DatatypeConfigurationException;
//import javax.xml.datatype.DatatypeFactory;
//import javax.xml.datatype.Duration;
//
//import org.eclipse.rdf4j.model.IRI;
//import org.eclipse.rdf4j.model.Model;
//import org.eclipse.rdf4j.model.util.Models;
//import org.eclipse.rdf4j.model.util.Values;
//import org.eclipse.rdf4j.rio.RDFFormat;
//import org.eclipse.rdf4j.rio.Rio;
//import org.junit.jupiter.api.Test;
//
//import de.naturzukunft.rdf4j.vocabulary.AS;
//import lombok.extern.slf4j.Slf4j;
//
//@Slf4j
//class TestConverter {
//
//	private static final IRI createActivity = AS.Create; 
//	private static final IRI asObject = AS.Object;
//		
//	@Test
//	void test() throws DatatypeConfigurationException {
//		log.trace("#################");
//		IRI subject = iri("http://test.de/subject");
//		IRI iri1 = iri("http://test.de/1");
//		IRI iri2 = iri("http://test.de/2");
//		IRI iri3 = iri("http://test.de/3");
//		String name = "name";
//		Byte byteValue = Byte.valueOf((byte)0xe0);
//		Short shortValue = Short.valueOf("2");
//		BigInteger myBigInt = new BigInteger("7777");
//		IRI iriReferenced = iri("http://someSubject.de");
//		String someAttribte = "testString";
//		ReferencedPojo referencedPojo = ReferencedPojo.builder().type(Set.of(asObject)).someAttribte(someAttribte).subject(iriReferenced).build();
//		LocalDateTime now = LocalDateTime.now();
//		TestPojo testPojo = TestPojo.builder()
//				.subject(subject)
//				.type(Set.of(createActivity))
//				.iri(iri2)
//				.iriSet(Set.of(iri1, iri2, iri3))
//				.name(name)
//				.myBoolean(Boolean.FALSE)
//				.myPrimitiveByte(byteValue)
//				.myByte(byteValue)
//				.myPrimitiveDouble(22.3)
//				.myDouble(22.3)
//				.myPrimitiveFloat(Float.parseFloat("22.2"))
//				.myFloat(Float.parseFloat("22.2"))
//				.myPrimitiveInt(1)
//				.myInt(1)
//				.myPrimitiveLong(Long.parseLong("66666"))
//				.myLong(Long.parseLong("44444"))
//				.myPrimitiveShort(shortValue)
//				.myShort(shortValue)
//				.myBigInt(myBigInt)
//				.referencedPojo(referencedPojo)
//				.duration(java.time.Duration.ofMinutes(5))
//				.localDateTime(now)
//				.build();
//				
//
//	ModelCreator<?> mc = new ModelCreator<>(testPojo);
//	Model model = mc.toModel();
//	
//	log.trace(toString(RDFFormat.TURTLE, model));
//	log.trace("subject = " + testPojo.getSubject());
//	String dt = Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "LocalDateTime")).get().stringValue();
//	LocalDateTime.parse(dt, DateTimeFormatter.ISO_DATE_TIME);
//	Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "LocalDateTime")).get();
//	
//	String duration = Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "Duration")).get().stringValue();
//	Duration d = DatatypeFactory.newInstance().newDuration(duration);
//	java.time.Duration timeDuration = java.time.Duration.ofMillis(d.getTimeInMillis(Calendar.getInstance()));
//	assertThat(timeDuration, is(java.time.Duration.ofMinutes(5)));
//	
//	assertThat(Models.getPropertyIRI(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "Iri")).get(), is(iri2));
//	assertThat(Models.getPropertyIRIs(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "IriSet")), is(Set.of(iri1,iri2,iri3)));
//	
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "String")).get().stringValue(), is(name));
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "Boolean")).get().booleanValue(), is(false));
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "byte")).get().byteValue(), is(byteValue));
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "Byte")).get().byteValue(), is(byteValue));
//
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "double")).get().doubleValue(), is(22.3));
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "Double")).get().doubleValue(), is(22.3));
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "float")).get().floatValue(), is(Float.parseFloat("22.2")));
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "Float")).get().floatValue(), is(Float.parseFloat("22.2")));
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "int")).get().intValue(), is(1));
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "Int")).get().intValue(), is(1));
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "long")).get().longValue(), is(66666L));
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "Long")).get().longValue(), is(44444L));
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "short")).get().shortValue(), is(shortValue));
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "Short")).get().shortValue(), is(shortValue));
//	assertThat(Models.getPropertyLiteral(model, testPojo.getSubject(), iri(TestPojo.NAMESPACE + "BigInteger")).get().integerValue(), is(myBigInt));
//	assertThat(Models.getPropertyLiteral(model, iriReferenced, iri(ReferencedPojo.NAMESPACE + "String")).get().stringValue(), is(someAttribte));
//
//	Converter<TestPojo> c = new Converter<TestPojo>(TestPojo.class);
//	TestPojo result = c.fromModel(subject, model).get();
//
//	// there are small differences due to the rounding. Therefore we cut off a few milliseconds
//	String convertedDateTime = result.getLocalDateTime().format(DateTimeFormatter.ISO_DATE_TIME);
//	String nowAsTrucatedString = now.format(DateTimeFormatter.ISO_DATE_TIME).substring(0, convertedDateTime.length());
//	assertThat(convertedDateTime, is(nowAsTrucatedString));
//	
//	assertThat(result.getDuration(), is(java.time.Duration.ofMinutes(5)));
//	assertThat(result.getIri(), is(iri2));
//	assertThat(result.getIriSet(), is(Set.of(iri1,iri2,iri3)));
//	assertThat(result.getMyBoolean(), is(false));
//	assertThat(result.getMyPrimitiveByte(), is(byteValue));
//	assertThat(result.getMyByte(), is(byteValue));
//
//	assertThat(result.getMyPrimitiveDouble(), is(22.3));
//	assertThat(result.getMyDouble(), is(22.3));
//	assertThat(result.getMyPrimitiveFloat(), is(Float.parseFloat("22.2")));
//	assertThat(result.getMyFloat(), is(Float.parseFloat("22.2")));
//	assertThat(result.getMyPrimitiveInt(), is(1));
//	assertThat(result.getMyInt(), is(1));
//	assertThat(result.getMyPrimitiveLong(), is(66666L));
//	assertThat(result.getMyLong(), is(44444L));
//	assertThat(result.getMyPrimitiveShort(), is(shortValue));
//	assertThat(result.getMyShort(), is(shortValue));
//	assertThat(result.getMyBigInt(), is(myBigInt));
//	assertThat(result.getReferencedPojo().getSomeAttribte(), is(referencedPojo.getSomeAttribte()));
//	log.trace(result.toString());
//
//	}
//	
//	@Test
//	void testNoneNull() {
//		IRI subject = iri("http://test.de/subject");
//		TestPojo testPojo = TestPojo.builder().subject(subject).name("Max").type(Set.of(createActivity)).build();
//		ModelCreator<?> mc = new ModelCreator<>(testPojo);
//		Model model = mc.toModel();
//		model.remove(subject, iri(TestPojo.NAMESPACE + "String"), Values.literal("Max"));
//		Converter<TestPojo> c = new Converter<TestPojo>(TestPojo.class);
//		Exception exception = assertThrows(NullPointerException.class, () -> {
//			c.fromModel(subject, model).get();
//		});
//		assertEquals(exception.getMessage(), "name is annotated with NonNull, but is null!");
//	}
//
//	@Test
//	void testModelCreatorNoneNull() {
//		IRI subject = iri("http://test.de/subject");
//		TestPojo testPojo = TestPojo.builder().subject(subject).type(Set.of(createActivity)).build();
//		ModelCreator<?> mc = new ModelCreator<>(testPojo);
//		Exception exception = assertThrows(NonNullException.class, () -> {
//			mc.toModel();
//		});
//		assertEquals(exception.getMessage(), "name is annotated with NonNull, but is null!");
//	}
//
//	@Test
//	void testNoneNullInReference() {
//		IRI iriReferenced = iri("http://someSubject.de");
//		ReferencedPojo referencedPojo = ReferencedPojo.builder().type(Set.of(asObject)).someAttribte("some").subject(iriReferenced).build();
//		IRI subject = iri("http://test.de/subject");
//		TestPojo testPojo = TestPojo.builder().subject(subject).type(Set.of(createActivity)).name("Max").referencedPojo(referencedPojo).build();
//		ModelCreator<?> mc = new ModelCreator<>(testPojo);
//		Model model = mc.toModel();
//		model.remove(iriReferenced, iri(ReferencedPojo.NAMESPACE + "String"), Values.literal("some"));
//		Converter<TestPojo> c = new Converter<TestPojo>(TestPojo.class);
//		Exception exception = assertThrows(NonNullException.class, () -> {
//			c.fromModel(subject, model).get();
//		});
//		assertEquals(exception.getMessage(), "someAttribte is annotated with NonNull, but is null!");
//	}
//
//	public static String toString(RDFFormat rdfFormat, Model model) {
//		StringWriter sw = new StringWriter();
//		Rio.write(model, sw, rdfFormat);		
//		return sw.toString();
//	}
//	
//}
