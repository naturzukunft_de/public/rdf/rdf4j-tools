package de.naturzukunft.rdf4j.ommapper;

public class NonNullException extends NullPointerException {
	private static final long serialVersionUID = 7263882305800344084L;
	public NonNullException(String s) {
		super(s);
	}
}
