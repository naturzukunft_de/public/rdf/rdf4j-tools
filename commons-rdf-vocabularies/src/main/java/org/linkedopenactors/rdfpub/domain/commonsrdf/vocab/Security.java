package org.linkedopenactors.rdfpub.domain.commonsrdf.vocab;

import org.apache.commons.rdf.api.IRI;

public interface Security extends Vocabulary {

	public static final String PREFIX = "sec";
	public static final String NAMESPACE = "https://w3id.org/security#";
	public static final String owner = "owner";
	public static final String publicKeyPem = "publicKeyPem";
	public static final String publicKey = "publicKey";
	
	IRI owner();

	IRI publicKeyPem();
	
	IRI publicKey();
}
