package org.linkedopenactors.rdfpub.domain.commonsrdf.vocab;

import org.apache.commons.rdf.api.IRI;

public interface XsdDatatypes extends Vocabulary {
	
	public static final String NS = "http://www.w3.org/2001/XMLSchema#";
	public final static String nonNegativeInteger = NS + "nonNegativeInteger";
	public final static String string = NS + "string";
	public final static String decimal = NS + "decimal";
	public final static String integer = NS + "integer";
	public final static String float_type = NS + "float";
	public final static String boolean_type = NS + "boolean";
	public final static String date = NS + "date";
	public final static String time = NS + "time";
	
	IRI nonNegativeInteger();
	IRI string();
	IRI decimal();
	IRI integer();
	IRI floatType();
	IRI booleanType();
	IRI date();
	IRI time();
}
