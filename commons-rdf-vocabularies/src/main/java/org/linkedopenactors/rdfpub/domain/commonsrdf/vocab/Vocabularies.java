package org.linkedopenactors.rdfpub.domain.commonsrdf.vocab;

public interface Vocabularies {
	AS getActivityStreams();

	Security getSecurity();
	
	RDFPUB getRdfPub();

	Rdf getRdf();

	LDP getLDP();
	
	XsdDatatypes getXsdDatatypes();
}
