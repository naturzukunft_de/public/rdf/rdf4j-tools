package de.naturzukunft.rdf4j.utils;

import java.io.StringWriter;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.slf4j.Logger;

public class ModelLogger {
	private enum LEVEL {
		INFO, TRACE, DEBUG, ERROR;
	}
	
	public static void trace(Logger logger, Model model, String...msg) {
		log(LEVEL.TRACE, logger, model, RDFFormat.TRIG, msg);
	}

	public static void debug(Logger logger, Model model, String...msg) {
		log(LEVEL.DEBUG, logger, model, RDFFormat.TRIG, msg);
	}
	
	public static void info(Logger logger, Model model, String...msg) {
		log(LEVEL.INFO, logger, model, RDFFormat.TRIG, msg);
	}

	public static void error(Logger logger, Model model, String...msg) {
		log(LEVEL.ERROR, logger, model, RDFFormat.TRIG, msg);
	}

	public static void trace(Logger logger, Model model, RDFFormat rdfFormat, String...msg) {
		log(LEVEL.TRACE, logger, model,rdfFormat,  msg);
	}

	public static void debug(Logger logger, Model model, RDFFormat rdfFormat, String...msg) {
		log(LEVEL.DEBUG, logger, model, rdfFormat, msg);
	}
	
	public static void info(Logger logger, Model model, RDFFormat rdfFormat, String...msg) {
		log(LEVEL.INFO, logger, model, rdfFormat, msg);
	}

	public static void error(Logger logger, Model model, RDFFormat rdfFormat, String...msg) {
		log(LEVEL.ERROR, logger, model, rdfFormat, msg);
	}

	private static void log(LEVEL level, Logger logger, Model model, RDFFormat rdfFormat, String...msg) {
		if(msg.length>1) {
			for (String string : msg) {
				log(level, logger, string);
			}
		} else if(msg.length==1) {
			log(level, logger, msg[0] + "\n" + toString(model, rdfFormat));
		} else {
			log(level, logger, toString(model, rdfFormat));
		}
	}
	
	private static void log(LEVEL level, Logger logger, String msg) {
		switch (level) {
		case INFO:
			logger.info(msg);
			break;

		case DEBUG:
			logger.debug(msg);
			break;

		case TRACE:
			logger.trace(msg);
			break;

		case ERROR:
			logger.error(msg);
			break;
		}
	}

	public static String toString(Model model) {
		return toString(model, RDFFormat.TRIG);
	}
	
	public static String toString(Model model, RDFFormat rdfFormat) {
		StringWriter sw = new StringWriter();
		if(model!=null) {
			Rio.write(model, sw, rdfFormat);
		}
		return sw.toString();
	}
}
