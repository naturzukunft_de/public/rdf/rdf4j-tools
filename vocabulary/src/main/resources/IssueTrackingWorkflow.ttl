@prefix : <http://www.w3.org/2000/01/rdf-schema#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix ui: <http://www.w3.org/ns/ui#> .

<http://test.de/> dc:title "Issue Tracking Ontology";
  dct:creator <http://www.w3.org/People/Berners-Lee/card#i>;
  :comment """This ontology defines a very general class (Task)
    which can used for any kind of bug tracking, issue tracking,
    to-do-list management, action items, goal dependency, and so on.
    It captures the state of a task as a subclass, so that
    subsumption can be used.
    It captures a discussion thread about a task.
    It captures subtasks structure if necessary.
    A \"Tracker\" defines actual set of states, categories, etc.,
    which  a task can be in. The data about the tracker
    guides the software managing the task.

    There is some workflow modeling finite state machine
    terms which are optional for  more complex definition
    of the transitions allowed.
    """ .

<http://usefulinc.com/ns/doap#bug-database> owl:inverseOf _:node1ev4lk3hax1 .

_:node1ev4lk3hax1 :label "project"@en .

<http://www.w3.org/2005/01/wf/flow#ActionItem> a :Class;
  :comment """An obligation taken on by a person, typically at a meeting.
    """;
  :label "action item"@en;
  :subClassOf <http://www.w3.org/2005/01/wf/flow#Task>;
  owl:disjointUnionOf _:node1ev4lk3hax2 .

_:node1ev4lk3hax2 rdf:first <http://www.w3.org/2005/01/wf/flow#Open>;
  rdf:rest _:node1ev4lk3hax3 .

_:node1ev4lk3hax3 rdf:first <http://www.w3.org/2005/01/wf/flow#Closed>;
  rdf:rest rdf:nil .

<http://www.w3.org/2005/01/wf/flow#Change> a :Class;
  :comment """The universal class of things which
change the state of a task.
Included now: Creation, Transition. (Maybe in the future
more π-calculus constructions such as splitting & merging tasks,
and import/export of obligations to a foreign opaque system.)
""";
  :label "change" .

<http://www.w3.org/2005/01/wf/flow#Closed> a :Class;
  :comment """A task which does not need attention. It may be closed because
        has been abandoned or completed, for example.
	""";
  :label "closed"@en, "fermé"@fr;
  :subClassOf <http://www.w3.org/2005/01/wf/flow#Task>;
  ui:backgroundColor "#f5d6d6" .

<http://www.w3.org/2005/01/wf/flow#Creation> a :Class;
  :comment """A creation is a change from existence
to non-existence
a task. Typical properties include date and/or source
(a document causing the transition), and a final state.""";
  :label "creation";
  :subClassOf <http://www.w3.org/2005/01/wf/flow#Change> .

<http://www.w3.org/2005/01/wf/flow#Message> a :Class;
  :label "message"@en .

<http://www.w3.org/2005/01/wf/flow#NonTerminalState> a :Class;
  :comment "A state from which there are transitions.";
  :label "non-terminal state";
  owl:disjointWith <http://www.w3.org/2005/01/wf/flow#TerminalState> .

<http://www.w3.org/2005/01/wf/flow#Open> a :Class;
  :comment """A task which needs attention. The very crude states of Open and Closed all
        interoperability between different systems if the states for a given
        application are made subclasses of either Open or Closed. This allows
        tasks from different systems to be mixed and treated together with
        limited but valuable functionality.
	""";
  :label "open"@en, "ouvert"@fr;
  :subClassOf <http://www.w3.org/2005/01/wf/flow#Task>;
  ui:backgroundColor "#d6f5d6" .

<http://www.w3.org/2005/01/wf/flow#Product> a :Class;
  :comment """A product is a task which monitors something
                which must be produced.""";
  :label "product";
  :subClassOf <http://www.w3.org/2005/01/wf/flow#Task> .

<http://www.w3.org/2005/01/wf/flow#Task> a :Class;
  :comment """Something to be done in a wide sense,
	an agenda item at a meeting is one example, but any
	issue, task, action item, goal, product, deliverable, milestone, can such a thing.
	The requirement for this framework was that it would allow
	one to customize ontologies for things such as agenda items,
	action items, working group issues with a spec, w3c Last Call issues,
	software bugs and administrative requests.
	In π-calculus, a process.
	Make your type of issue a subclass of Task.
	""";
  :label "task"@en;
  owl:disjointUnionOf _:node1ev4lk3hax4 .

_:node1ev4lk3hax4 rdf:first <http://www.w3.org/2005/01/wf/flow#Open>;
  rdf:rest _:node1ev4lk3hax5 .

_:node1ev4lk3hax5 rdf:first <http://www.w3.org/2005/01/wf/flow#Closed>;
  rdf:rest rdf:nil .

<http://www.w3.org/2005/01/wf/flow#TerminalState> a :Class;
  :comment "A state from which there are no transitions.";
  :label "terminal state";
  :subClassOf <http://www.w3.org/2005/01/wf/flow#State> .

<http://www.w3.org/2005/01/wf/flow#Tracker> a :Class;
  :comment """A set of issues and
                the constraints on how they evolve.
                To use this ontology, craete a new tracker.
                Copy an existing one or make up your own.""";
  :label "tracker" .

<http://www.w3.org/2005/01/wf/flow#Transition> a :Class;
  :comment """A transition is a change of state of
a task. Typical properties include date and/or source
(a document causing the transition), and a final state.""";
  :label "transition";
  :subClassOf <http://www.w3.org/2005/01/wf/flow#Change> .

<http://www.w3.org/2005/01/wf/flow#affects> a rdf:Property;
  :domain <http://www.w3.org/2000/10/swap/pim/doc#Work>;
  :label "affects";
  :range <http://www.w3.org/2005/01/wf/flow#Task> .

<http://www.w3.org/2005/01/wf/flow#allowedTransitions> a rdf:Property;
  :comment """The state machine is defined
	by these lists of transition allowed for each issue.
	(An interesting option in the Web is to make an allowed transition
	to a state in soemone else's ontology, which in turn allows
	transitions into many ontologies.  So a finite state maxchine
	may become very large. In practice this means that a task handed
	off to another organization may be processed on all kinds of ways.)""";
  :domain <http://www.w3.org/2005/01/wf/flow#State>;
  :label "allowed transitions";
  :range rdf:List .

<http://www.w3.org/2005/01/wf/flow#asigneeClass> a rdf:Property;
  :comment "When an issue is assigned, the assignee must be from this class";
  :domain <http://www.w3.org/2005/01/wf/flow#Tracker>;
  :label "assignees must be";
  :range :Class .

<http://www.w3.org/2005/01/wf/flow#assignee> a rdf:Property;
  :comment "The person or group to whom this has been assigned.";
  :label "assigned to";
  :range <http://xmlns.com/foaf/0.1/Agent>;
  owl:inverseOf _:node1ev4lk3hax6 .

_:node1ev4lk3hax6 :label "assignment" .

<http://www.w3.org/2005/01/wf/flow#attachment> a rdf:Property;
  :comment "Something related is attached for information.";
  :label "attachment" .

<http://www.w3.org/2005/01/wf/flow#creates> a rdf:Property;
  :domain <http://www.w3.org/2000/10/swap/pim/doc#Work>;
  :label "creates";
  :range <http://www.w3.org/2005/01/wf/flow#Task> .

<http://www.w3.org/2005/01/wf/flow#date> :range <http://www.w3.org/2005/01/wf/flow#DateTime> .

<http://www.w3.org/2005/01/wf/flow#dateDue> a rdf:Property, owl:DatatypeProperty;
  :comment """The date this task is due.
                """;
  :domain <http://www.w3.org/2005/01/wf/flow#Task>;
  :label "due"@en;
  :range <http://www.w3.org/2001/XMLSchema#date> .

<http://www.w3.org/2005/01/wf/flow#deliverable> a rdf:Property;
  :comment "Something which must be delivered to accomplish this";
  :label "deliverable"@en;
  :range <http://www.w3.org/2005/01/wf/flow#Product>;
  :subPropertyOf <http://www.w3.org/2005/01/wf/flow#dependent> .

<http://www.w3.org/2005/01/wf/flow#dependent> a rdf:Property;
  :comment """Another task upon which this depends, in the sense that
        this task cannot be completed without that task being done.
        You can't use this for dependencies on anything other than other tasks.
        (Note the US spelling of the URI. In the UK, a dependant is a something
        which is dependent on somehing else.)""";
  :domain <http://www.w3.org/2005/01/wf/flow#Task>;
  :label "how";
  :range <http://www.w3.org/2005/01/wf/flow#Task>;
  owl:inverseOf _:node1ev4lk3hax7 .

_:node1ev4lk3hax7 :label "why" .

<http://www.w3.org/2005/01/wf/flow#description> a rdf:Property;
  :comment """The description, definition,
        or abstract. Information explaining what this is.
        Not arbitrary comment about anything, only about the subject.
        (Use this property for anything. There is no domain restriction.).""";
  :label "description" .

<http://www.w3.org/2005/01/wf/flow#final> a rdf:Property;
  :domain <http://www.w3.org/2005/01/wf/flow#Transition>;
  :label "to";
  :range <http://www.w3.org/2005/01/wf/flow#State> .

<http://www.w3.org/2005/01/wf/flow#goalDescription> a rdf:Property, owl:DatatypeProperty;
  :comment "A textual description of the goals of this product, etc.";
  :domain <http://www.w3.org/2005/01/wf/flow#Task>;
  :label "goals";
  :range <http://www.w3.org/2001/XMLSchema#string> .

<http://www.w3.org/2005/01/wf/flow#initialState> a rdf:Property;
  :comment "The initial state for a new issue";
  :domain <http://www.w3.org/2005/01/wf/flow#Tracker>;
  :label "initial state"@en, "état initial"@fr;
  :range <http://www.w3.org/2005/01/wf/flow#State> .

<http://www.w3.org/2005/01/wf/flow#issue> a rdf:Property;
  :comment "A transition changes the state of the given issue.";
  :label "issue" .

<http://www.w3.org/2005/01/wf/flow#issueCategory> a rdf:Property;
  :comment """Issues may be categorized according to the
                subclasses of this class""";
  :domain <http://www.w3.org/2005/01/wf/flow#Tracker>;
  :label "issue category";
  :range :Class .

<http://www.w3.org/2005/01/wf/flow#issueClass> a rdf:Property;
  :comment """The class of issues which are allowed in this tracker.
                This is essemtial to the operation of the tracker,
                as it defines which states an issue can be in.
                (The issueClass must be a disjointUnionOf the state classes)""";
  :domain <http://www.w3.org/2005/01/wf/flow#Tracker>;
  :label "all issues must be in";
  :range :Class, <http://www.w3.org/2005/01/wf/flow#State> .

<http://www.w3.org/2005/01/wf/flow#messageProp> a rdf:Property;
  :comment "A message about this. Attached for information.";
  :label "message"@en;
  :subPropertyOf <http://www.w3.org/2005/01/wf/flow#attachment> .

<http://www.w3.org/2005/01/wf/flow#modifiedBy> a rdf:Property;
  :label "changed by";
  :range <http://xmlns.com/foaf/0.1/Agent> .

<http://www.w3.org/2005/01/wf/flow#recipent> a rdf:Property;
  :domain <http://www.w3.org/2005/01/wf/flow#Message>;
  :label "to";
  :range <http://xmlns.com/foaf/0.1/Agent> .

<http://www.w3.org/2005/01/wf/flow#requires> a rdf:Property;
  :comment """To be a valid transition,
		a necessary (but not necessarily sufficuent) condition
		is that there be recorded these properties for the record""";
  :domain <http://www.w3.org/2005/01/wf/flow#Transition>;
  :label "requires";
  :range rdf:List .

<http://www.w3.org/2005/01/wf/flow#screenShot> a rdf:Property;
  :comment """An image taken by capturing the state of a
                 computer screen, for example to demonstrate a problem""";
  :label "screen shot"@en;
  :subPropertyOf <http://www.w3.org/2005/01/wf/flow#attachment> .

<http://www.w3.org/2005/01/wf/flow#sender> a rdf:Property;
  :domain <http://www.w3.org/2005/01/wf/flow#Message>;
  :label "from";
  :range <http://xmlns.com/foaf/0.1/Agent> .

<http://www.w3.org/2005/01/wf/flow#source> a rdf:Property;
  :comment """The source of a transition is
				the document by which it happened""";
  :label "source";
  :range <http://www.w3.org/2000/10/swap/pim/doc#Work> .

<http://www.w3.org/2005/01/wf/flow#stateStore> a rdf:Property;
  :comment """A read-write document.
                The state of the issues is modified here.
                When you set up a trcaker, thgis must be set to point
                to a writeble data resource on the web.""";
  :domain <http://www.w3.org/2005/01/wf/flow#Tracker>;
  :label "state store";
  :range <http://www.w3.org/2000/10/swap/pim/doc#Document> .

<http://www.w3.org/2005/01/wf/flow#subscriber> a rdf:Property;
  :label "subscriber";
  :range <http://xmlns.com/foaf/0.1/Agent> .

<http://www.w3.org/2005/01/wf/flow#successCriteria> a rdf:Property, owl:DatatypeProperty;
  :comment """A textual description of the successs critera.
                How when we know this is done?""";
  :domain <http://www.w3.org/2005/01/wf/flow#Task>;
  :label "success criteria";
  :range <http://www.w3.org/2001/XMLSchema#string> .

<http://www.w3.org/2005/01/wf/flow#taskProp> a rdf:Property;
  :label "task";
  :range <http://www.w3.org/2005/01/wf/flow#Task> .

<http://www.w3.org/2005/01/wf/flow#terminalOutput> a rdf:Property;
  :comment """A file showing user interaction from a
                text terminal or console etc. """;
  :label "terminal output"@en;
  :subPropertyOf <http://www.w3.org/2005/01/wf/flow#attachment> .

<http://www.w3.org/2005/01/wf/flow#testData> a rdf:Property;
  :comment """A file which can be used as inpiut to a test
                or to demonstrate a problem. """;
  :label "test data"@en;
  :subPropertyOf <http://www.w3.org/2005/01/wf/flow#attachment> .

<http://www.w3.org/2005/01/wf/flow#trackerProp> a rdf:Property;
  :domain <http://www.w3.org/2005/01/wf/flow#Task>;
  :label "tracker";
  :range <http://www.w3.org/2005/01/wf/flow#Tracker>;
  owl:inverseOf _:node1ev4lk3hax8 .

_:node1ev4lk3hax8 :label "issue" .

<http://www.w3.org/2005/01/wf/flow#transactionStore> a rdf:Property;
  :comment """An appendable document. Transactions and messsages
                    can be written into here""";
  :domain <http://www.w3.org/2005/01/wf/flow#Tracker>;
  :label "transaction store";
  :range <http://www.w3.org/2000/10/swap/pim/doc#Document> .