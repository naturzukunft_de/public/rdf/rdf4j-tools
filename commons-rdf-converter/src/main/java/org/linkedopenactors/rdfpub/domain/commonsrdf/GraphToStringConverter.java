package org.linkedopenactors.rdfpub.domain.commonsrdf;

import org.apache.commons.rdf.api.Graph;

public interface GraphToStringConverter {
	String convert(RdfFormat rdfFormat, Graph graph);
	
	/**
	 * @param rdfFormat
	 * @param graph
	 * @param resolveIris True, if the internal iris should be replaced with external https:// urls.
	 * @return The grap as string.
	 */
	String convert(RdfFormat rdfFormat, Graph graph, boolean resolveIris);
}
