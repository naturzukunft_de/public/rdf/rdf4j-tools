package org.linkedopenactors.rdfpub.store.rdf4j;

import java.io.StringWriter;

import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.rio.Rio;
import org.linkedopenactors.rdfpub.domain.commonsrdf.GraphToStringConverter;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
class GraphToStringConverterDefault implements GraphToStringConverter {

	private RDF4J rdf4j;
	private PrefixMapper subjectProvider;

	public GraphToStringConverterDefault(PrefixMapper subjectProvider) {
		this.subjectProvider = subjectProvider;
		rdf4j = new RDF4J();
	}
	
	@Override
	public String convert(RdfFormat rdfFormat, Graph graph) {
		return convert(rdfFormat, graph, false);
	}

	@Override
	public String convert(RdfFormat rdfFormat, Graph graph, boolean resolveIris) {
		Model asModel = asModel(graph);
		return convert(rdfFormat, asModel, resolveIris);
	}
	
//	public String convert(RdfFormat rdfFormat, Model asModel) {
//		return convert(rdfFormat, asModel, false);
//	}
	
	private String convert(RdfFormat rdfFormat, Model asModel, boolean resolveIris) {		
		StringWriter sw = new StringWriter();		
		Rio.write(asModel, sw, RdfFormatConverter.toRdf4j(rdfFormat));
		String asString = sw.toString();
		
		if(resolveIris) {
			if(subjectProvider==null) {
				log.warn("subjectProvider in null! Unable to resolve iris !!");
			} else {
				String internaleIriPrefix = subjectProvider.getInternaleIriPrefix();
				String externalIriPrefix = subjectProvider.getExternalIriPrefix();
				externalIriPrefix = externalIriPrefix.endsWith("/") ? externalIriPrefix : externalIriPrefix + "/";
				asString = asString.replaceAll(internaleIriPrefix, externalIriPrefix);		
			}
		}				
		if(RdfFormat.JSONLD.equals(rdfFormat)) {
			asString = new CompacterDefault().compact(asString);
		}
		return asString;
	}

	private Model asModel(Graph graph) {
		Model model = new ModelBuilder().build();
		graph.stream().forEach(t->model.add(rdf4j.asStatement(t)));
		return model;
	}
}
