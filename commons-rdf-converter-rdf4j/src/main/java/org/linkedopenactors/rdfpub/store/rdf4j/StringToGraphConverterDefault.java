package org.linkedopenactors.rdfpub.store.rdf4j;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.eclipse.rdf4j.rio.helpers.JSONLDSettings;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.commonsrdf.StringToGraphConverter;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class StringToGraphConverterDefault implements StringToGraphConverter {

	private RDF4J rdf4j;

	public StringToGraphConverterDefault(RDF4J rdf4j) {
		this.rdf4j = rdf4j;		
	}
	
	@Override
	public Graph convert(RdfFormat rdfFormat, String graphAsString) {
		Model model = asModel(rdfFormat, graphAsString);
		return rdf4j.asGraph(model);
	}

	@Override
	public Map<BlankNodeOrIRI, Graph> convert(RdfFormat rdfFormat, Set<BlankNodeOrIRI> subjects, String graphAsString) {
		Map<BlankNodeOrIRI, Graph> result = new HashMap<>();		
		
		Model model = asModel(rdfFormat, graphAsString);		
		
		subjects.forEach(s->{
			Model filtered = model.filter(Values.iri(s.toString()), null, null);
			result.put(s, rdf4j.asGraph(filtered));
		});
		return result;
	}

	private Model asModel(RdfFormat rdfFormat, String graphAsString) {
		Model model;
		try { 	RDFFormat format = RdfFormatConverter.toRdf4j(rdfFormat);
			RDFParser parser = Rio.createParser(format);
			model = new LinkedHashModel();
			parser.setRDFHandler(new StatementCollector(model));
			parser.getParserConfig().set(JSONLDSettings.SECURE_MODE, false);
			parser.parse(new StringReader(graphAsString));
			// TODO is there a way to extend the whitelist ? See: https://github.com/eclipse-rdf4j/rdf4j/discussions/5161
//			model = Rio.parse(new StringReader(graphAsString), format);
		} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
			log.error("error converting the following graph to " + rdfFormat + " : " + graphAsString, e);
			throw new IllegalArgumentException("Problem converting to a graph.", e);
		}
		return model;
	}
}
