package org.linkedopenactors.rdfpub.store.rdf4j;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.eclipse.rdf4j.rio.helpers.JSONLDSettings;
import org.springframework.stereotype.Component;

import com.github.jsonldjava.core.JsonLdError;
import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.utils.JsonUtils;

@Component
class CompacterDefault {
	
	public String compact(Model input) throws IOException, JsonLdError {
		return compact(input, true);
	}
	
	public String compact(Model input, boolean pretty) throws IOException, JsonLdError {
		WriterConfig config = getWriterConfig();
		StringWriter stringWriter = new StringWriter();
		Rio.write(input, stringWriter, RDFFormat.JSONLD, config);
		String string = stringWriter.toString();
		return compactInternal(string, pretty); 
	}

	@SuppressWarnings("removal")
	private WriterConfig getWriterConfig() {
		WriterConfig config = new WriterConfig();
		config.set(JSONLDSettings.HIERARCHICAL_VIEW, true);
		config.set(JSONLDSettings.COMPACT_ARRAYS, true);
		config.set(JSONLDSettings.OPTIMIZE, true);
		return config;
	}
	
	public String compact(String input) {
		return compact(input, true);
	}
	
	public String compact(String jsonldString, boolean pretty) {
		Model input;
		try {
			input = Rio.parse(new StringReader(jsonldString), RDFFormat.JSONLD);
		} catch (Exception e) {
			throw new IllegalStateException("cannot parse jsonld: " + jsonldString, e);
		}		
		WriterConfig config = getWriterConfig();
		StringWriter stringWriter = new StringWriter();
		Rio.write(input, stringWriter, RDFFormat.JSONLD, config);
		String string = stringWriter.toString();
		return compactInternal(string, pretty); 
	}
	
	
	private String compactInternal(String input, boolean pretty) {
		try {
			JsonLdOptions opts = new JsonLdOptions();
			opts.setCompactArrays(true);
//		opts.setOmitDefault(Boolean.TRUE);
			opts.setProcessingMode(JsonLdOptions.JSON_LD_1_1);

			String context = getContext();
			Object frame = JsonUtils.fromString(context);
			Map<String, Object> res = JsonLdProcessor.compact(JsonUtils.fromString(input), frame, opts);

			StringWriter writer = new StringWriter();
			if(pretty) {
				JsonUtils.writePrettyPrint(writer, res);
			} else {
				JsonUtils.write(writer, res);
			}
			return writer.toString();
		} catch (Exception e) {
			throw new IllegalStateException("unable to compact json-ld", e);
		}
	}

	private String getContext() {
		return "{\n" + "  \"@context\": [\n" + "  	\"https://schema.org/docs/jsonldcontext.json\",\n"
				+ "  	\"https://rdf-pub.org/schema/rdf-pub-context.json\",\n"
				+ "  	\"https://w3id.org/security/v1\",\n"				
				+ "  	\"https://www.w3.org/ns/activitystreams\"\n"
				+ "  ]\n" + "}";

	}
}
