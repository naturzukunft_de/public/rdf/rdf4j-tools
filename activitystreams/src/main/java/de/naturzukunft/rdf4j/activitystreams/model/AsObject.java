package de.naturzukunft.rdf4j.activitystreams.model;

import java.time.LocalDateTime;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;

import de.naturzukunft.rdf4j.ommapper.BaseObject;
import de.naturzukunft.rdf4j.ommapper.Iri;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper=true, includeFieldNames=true)
@Iri(AS.NAMESPACE)
public class AsObject extends BaseObject {

	/**
	 * Identifies an entity considered to be part of the public primary audience of an Object
	 * @See https://www.w3.org/ns/activitystreams#to 
	 */
	@Iri(AS.TO_STRING)
	private Set<IRI> to;

	/**
	 * Identifies an Object that is part of the private primary audience of this Object.
	 * @See https://www.w3.org/ns/activitystreams#bto 
	 */
	@Iri(AS.BTO_STRING)
	private Set<IRI> bto;

	/**
	 * Identifies an Object that is part of the public secondary audience of this Object. 
	 * @See https://www.w3.org/ns/activitystreams#cc
	 */
	@Iri(AS.CC_STRING)
	private Set<IRI> cc;

	/**
	 * Identifies one or more Objects that are part of the private secondary audience of this Object. 
	 * @See https://www.w3.org/ns/activitystreams#bcc
	 */
	@Iri(AS.BCC_STRING)
	private Set<IRI> bcc;

	/**
	 * Identifies one or more entities that represent the total population of entities for which the object can considered to be relevant. 
	 * @See https://www.w3.org/ns/activitystreams#audience
	 */
	@Iri(AS.AUDIENCE_STRING)
	private Set<IRI> audience;

	/**
	 * Identifies one or more entities to which this object is attributed. The
	 * attributed entities might not be Actors. For instance, an object might be
	 * attributed to the completion of another activity.
	 * @See https://www.w3.org/ns/activitystreams#attributedTo
	 */
	@Iri(AS.ATTRIBUTED_TO_STRING)
	private Set<IRI> attributedTo;
	
	/**
	 * A simple, human-readable, plain-text name for the object. HTML markup MUST
	 * NOT be included. The name MAY be expressed using multiple language-tagged
	 * values.
	 * @See https://www.w3.org/ns/activitystreams#name
	 */
	@Iri(AS.NAME_STRING)
	private String asName;
	
	/**
	 * Identifies a resource attached or related to an object that potentially
	 * requires special handling. The intent is to provide a model that is at least
	 * semantically similar to attachments in email.
	 * @See https://www.w3.org/ns/activitystreams#attachment
	 */
	@Iri(AS.ATTACHMENT_STRING)
	private Set<IRI> attachment;
	
	/**
	 * The content or textual representation of the Object encoded as a JSON string.
	 * By default, the value of content is HTML. The mediaType property can be used
	 * in the object to indicate a different content type. The content MAY be
	 * expressed using multiple language-tagged values.
	 * @See https://www.w3.org/ns/activitystreams#content
	 */
	@Iri(AS.CONTENT_STRING)
	private String content;
	
	/**
	 * Identifies the context within which the object exists or an activity was
	 * performed. The notion of "context" used is intentionally vague. The intended
	 * function is to serve as a means of grouping objects and activities that share
	 * a common originating context or purpose. An example could be all activities
	 * relating to a common project or event.
	 * @See https://www.w3.org/ns/activitystreams#context
	 */
	@Iri(AS.CONTEXT_STRING)
	private IRI context;
	
	/**
	 * The date and time describing the actual or expected ending time of the
	 * object. When used with an Activity object, for instance, the endTime property
	 * specifies the moment the activity concluded or is expected to conclude.
	 * @See https://www.w3.org/ns/activitystreams#endTime
	 */
	@Iri(AS.END_TIME_STIRNG)
	private LocalDateTime endTime;
	
	/**
	 * Identifies the entity (e.g. an application) that generated the object. 
	 * @See https://www.w3.org/ns/activitystreams#generator
	 */
	@Iri(AS.GENERATOR_STRING)
	private IRI generator;
	
	/**
	 * Indicates an entity that describes an icon for this object. The image should
	 * have an aspect ratio of one (horizontal) to one (vertical) and should be
	 * suitable for presentation at a small size.
	 * @See https://www.w3.org/ns/activitystreams#icon
	 */
	@Iri(AS.ICON_STRING)
	private IRI icon;
	
	/**
	 * An image document of any kind
	 * @See https://www.w3.org/ns/activitystreams#image
	 */
	@Iri(AS.IMAGE_STRING)
	private Set<IRI> image;
	
	/**
	 * Indicates one or more entities for which this object is considered a response. 
	 * @See https://www.w3.org/ns/activitystreams#inReplyTo
	 */
	@Iri(AS.IN_REPLY_TO_STRING)
	private Set<IRI> inReplyTo;
	
	/**
	 * Indicates one or more physical or logical locations associated with the object. 
	 * @See https://www.w3.org/ns/activitystreams#location
	 */
	@Iri(AS.LOCATION_STRING)
	private Set<IRI> location;
	
	/**
	 * Identifies an entity that provides a preview of this object. 
	 * @See https://www.w3.org/ns/activitystreams#preview
	 */
	@Iri(AS.PREV_STRING)
	private IRI preview;
	
	/**
	 * The date and time at which the object was published 
	 * @See https://www.w3.org/ns/activitystreams#published
	 */
	@Iri(AS.PUBLISHED_STRING)
	private LocalDateTime published;
	
	/**
	 * Identifies a Collection containing objects considered to be responses to this object. 
	 * @See https://www.w3.org/ns/activitystreams#replies
	 */
	@Iri(AS.REPLIES_STRING)
	private IRI replies;
	
	/**
	 * The date and time describing the actual or expected starting time of the
	 * object. When used with an Activity object, for instance, the startTime
	 * property specifies the moment the activity began or is scheduled to begin.
	 * @See https://www.w3.org/ns/activitystreams#startTime
	 */
	@Iri(AS.START_TIME_STRING)
	private LocalDateTime startTime;
	
	/**
	 * A natural language summarization of the object encoded as HTML. Multiple language tagged summaries MAY be provided. 
	 * @See https://www.w3.org/ns/activitystreams#summary
	 */
	@Iri(AS.SUMMARY_STRING)
	private String summary;
	
	/**
	 * One or more "tags" that have been associated with an objects. A tag can be
	 * any kind of Object. The key difference between attachment and tag is that the
	 * former implies association by inclusion, while the latter implies associated
	 * by reference.
	 * @See https://www.w3.org/ns/activitystreams#tag
	 */
	@Iri(AS.TAG_STRING)
	private Set<IRI> tag;
	
	/**
	 * The date and time at which the object was updated 
	 * @See https://www.w3.org/ns/activitystreams#updated
	 */
	@Iri(AS.UPDATED_STRING)
	private LocalDateTime updated;
	
	/**
	 * Identifies one or more links to representations of the object 
	 * @See https://www.w3.org/ns/activitystreams#url
	 */
	@Iri(AS.URL_STRING)
	private Set<IRI> url;
	
	/**
	 * When used on a Link, identifies the MIME media type of the referenced
	 * resource. When used on an Object, identifies the MIME media type of the value
	 * of the content property. If not specified, the content property is assumed to
	 * contain text/html content.
	 * @See https://www.w3.org/ns/activitystreams#mediaType
	 */
	@Iri(AS.MEDIA_TYPE_STRING)
	private String mediaType;
	
	/**
	 * When the object describes a time-bound resource, such as an audio or video, a
	 * meeting, etc, the duration property indicates the object's approximate
	 * duration. The value MUST be expressed as an xsd:duration as defined by [
	 * xmlschema11-2], section 3.3.6 (e.g. a period of 5 seconds is represented as
	 * "PT5S").
	 * @See https://www.w3.org/ns/activitystreams#duration
	 */
	@Iri(AS.DURATION_STRING)
	private java.time.Duration duration;
}


