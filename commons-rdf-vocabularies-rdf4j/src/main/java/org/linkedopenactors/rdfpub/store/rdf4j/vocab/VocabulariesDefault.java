package org.linkedopenactors.rdfpub.store.rdf4j.vocab;

import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.AS;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.LDP;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.RDFPUB;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.Rdf;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.Security;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.Vocabularies;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.XsdDatatypes;
import org.springframework.stereotype.Component;

@Component
public class VocabulariesDefault implements Vocabularies {

	@Override
	public AS getActivityStreams() {
		return new ASDefault();
	}

	@Override
	public Security getSecurity() {
		return new SecurityDefault();
	}

	@Override
	public RDFPUB getRdfPub() {
		return new RDFPUBDefault();
	}

	@Override
	public Rdf getRdf() {
		return new RdfDefault();
	}

	@Override
	public LDP getLDP() {
		return new LDPDefault();
	}

	@Override
	public XsdDatatypes getXsdDatatypes() {
		return new XsdDatatypesDefault();
	}
}
