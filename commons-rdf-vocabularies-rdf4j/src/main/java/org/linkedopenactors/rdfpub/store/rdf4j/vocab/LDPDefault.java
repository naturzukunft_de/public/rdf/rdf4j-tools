package org.linkedopenactors.rdfpub.store.rdf4j.vocab;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.LDP;

public class LDPDefault implements LDP {

	private RDF4J rdf;

	public LDPDefault() {
		rdf = new RDF4J();
	}

	@Override
	public IRI getNamespace() {
		return rdf.createIRI(LDP.NS);
	}

	@Override
	public IRI inbox() {
		return rdf.createIRI(LDP.inbox);
	}
}
