package org.linkedopenactors.rdfpub.store.rdf4j.vocab;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.XsdDatatypes;

public class XsdDatatypesDefault implements XsdDatatypes {

	private RDF rdf;

	public XsdDatatypesDefault() { 
		rdf = new RDF4J();
	}
	
	@Override
	public IRI getNamespace() {
		return rdf.createIRI(NS);
	}

	@Override
	public IRI nonNegativeInteger() {
		return rdf.createIRI(nonNegativeInteger);
	}

	@Override
	public IRI string() {
		return rdf.createIRI(string);
	}

	@Override
	public IRI decimal() {
		return rdf.createIRI(decimal);
	}

	@Override
	public IRI integer() {
		return rdf.createIRI(integer);
	}

	@Override
	public IRI floatType() {
		return rdf.createIRI(float_type);
	}

	@Override
	public IRI booleanType() {
		return rdf.createIRI(boolean_type);
	}

	@Override
	public IRI date() {
		return rdf.createIRI(date);
	}

	@Override
	public IRI time() {
		return rdf.createIRI(time);
	}
}
