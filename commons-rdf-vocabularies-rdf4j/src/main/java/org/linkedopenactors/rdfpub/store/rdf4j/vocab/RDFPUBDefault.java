package org.linkedopenactors.rdfpub.store.rdf4j.vocab;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.RDFPUB;

public class RDFPUBDefault implements org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.RDFPUB {

	private RDF4J rdf;

	public RDFPUBDefault() {
		rdf = new RDF4J();
	}

	@Override
	public IRI getNamespace() {
		return rdf.createIRI(RDFPUB.NS);
	}

	@Override
	public IRI commitId() {
		return rdf.createIRI(RDFPUB.commitId);
	}

	@Override
	public IRI oauth2IssuerUserId() {
		return rdf.createIRI(RDFPUB.oauth2IssuerUserId);
	}

	@Override
	public IRI version() {
		return rdf.createIRI(RDFPUB.version);
	}
}
