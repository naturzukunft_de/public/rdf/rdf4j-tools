package org.linkedopenactors.rdfpub.store.rdf4j.vocab;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.Security;

public class SecurityDefault implements Security {

	private RDF4J rdf;

	public SecurityDefault() {
		rdf = new RDF4J();
	}

	@Override
	public IRI getNamespace() {
		return rdf.createIRI(Security.NAMESPACE + Security.NAMESPACE);
	}

	@Override
	public IRI owner() {
		return rdf.createIRI(Security.NAMESPACE + Security.owner);
	}

	@Override
	public IRI publicKeyPem() {
		return rdf.createIRI(Security.NAMESPACE + Security.publicKeyPem);
	}

	@Override
	public IRI publicKey() {
		return rdf.createIRI(Security.NAMESPACE + Security.publicKey);
	}
}
